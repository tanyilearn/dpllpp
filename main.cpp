#include <getopt.h>
#include "Formula.h"
#include "DPLL.h"
#include "loader.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <iomanip>

void helpScreen() {
    printf("dpll [option] <-a | <cnf_file(s)>>");
    printf("\n\t-a\t\t\t Run tests on all *.cnf files in current directory.");
    printf("\n\t\t\t\t Without -a option CNF file must be specified.");
    printf("\n\t-s\t\t\t Silent mode (no screen output)");
    printf("\n\t-l FILENAME\t Specify filename for logfile.\n");
}

void toLower(std::string &s) {
    for (char & it : s) {
        it = tolower(it);
    }
}

std::string dateTime() {
    std::time_t t = std::time(0);   // get time now
    std::tm* now = std::localtime(&t);
    std::stringstream output;
    output << (now->tm_year + 1900) << '-'
            << std::setw(2) << std::setfill('0') << (now->tm_mon + 1) << '-'
            << std::setw(2) << std::setfill('0') << now->tm_mday << '_'
            << std::setw(2) << std::setfill('0') << (now->tm_hour) << '-'
            << std::setw(2) << std::setfill('0') << (now->tm_min) << '-'
            << std::setw(2) << std::setfill('0') << (now->tm_sec);
    return output.str();
}

int main(int argc, char** argv) {

    // Handling command line arguments
    if (argc <= 1) {
        helpScreen();
        exit(1);
    }

    bool allCnf, silent;
    std::string logfilename = "Log_";
    int c;

    allCnf = false;
    silent = false;
    logfilename.append(dateTime().append(".log"));
    while ((c = getopt(argc, argv, "hasl:")) != -1) {
        switch(c) {
            case 'a':
                allCnf = true;
                break;
            case 'l':
                logfilename = optarg;
                break;
            case 's':
                silent = true;
                break;
            case 'h':
            case '?':
            default:
                helpScreen();
        }
    }

    // Getting filenames
    std::vector<std::string> filenames;
    if (allCnf) {
        for (boost::filesystem::directory_iterator itr("."); itr != boost::filesystem::directory_iterator(); ++itr) {

            std::string extension = itr->path().extension().string();
            toLower(extension);
            if (extension == ".cnf")
                filenames.push_back(itr->path().filename().c_str());
        }
    } else if (argc > optind) {
        int argCount = optind;
        while (argCount < argc) {
            filenames.emplace_back(argv[argCount]);
            argCount++;
        }
    } else {
        helpScreen();
        exit(1);
    }

    // Processing files
    for (std::string file : filenames) {
        Formula f;
        try {
            load(file, f);
        } catch (std::exception e) {
            std::cout << "Can't load file: " << file << std::endl;
            exit(1);
        }

        // Running - time measurement
        std::stringstream output;
        auto start = std::chrono::high_resolution_clock::now();
        bool sat = dpll(f);
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        // Generate output
        if (file.substr(0,2)  == "uf" && sat || file.substr(0,3)  == "uuf" && !sat)
            output << "OK:    ";
        else
            output << "ERROR: ";
        output << file << '\t';
        if (sat)
            output << "SATISFIABLE";
        else
            output << "UNSATISFIABLE";
        output << " running time: " << duration.count() / 1000000.0 << "s\n";
        if (!silent)
            std::cout << output.str();

        // Logging
        std::ofstream ofs(logfilename, std::ios_base::binary);
        ofs << output.str();
        ofs.close();

    }

    return 0;
}
