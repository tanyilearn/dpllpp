//
// Created by tanyi on 2022.08.06..
//

#ifndef DPLL_FORMULA_H
#define DPLL_FORMULA_H

#include <set>
#include <algorithm>
#include "Types.h"
#include "utils.h"


class Formula {
private:
    std::vector<fClause> clauses;

    std::set<fVariable> variables;

public:
    void addClause(fClause clause);

    void addLiteral(fLiteral literal);

//    void removeClause(fClause clause);

    std::vector<fClause> &getClauses();

    fVariable pickVariable();

};


#endif //DPLL_FORMULA_H
