//
// Created by tanyi on 2022.08.08..
//

#ifndef DPLL_LOADER_H
#define DPLL_LOADER_H

#include <fstream>
#include "Formula.h"

void load(const std::string &filename, Formula &formula);

bool isNumber(std::string &line);

#endif //DPLL_LOADER_H
