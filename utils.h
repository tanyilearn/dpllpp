//
// Created by tanyi on 2022.08.07..
//

#ifndef DPLL_UTILS_H
#define DPLL_UTILS_H

#include <set>
#include <fstream>
#include <sstream>
#include <regex>
//#include "Formula.h"

//template <typename T>
//T popFromSet(std::set<T> &vec);

template <typename T>
T popFromSet(std::set<T> &vec) {
    typename std::set<T>::iterator it;
    it = prev(vec.end());
    T picked = *it;
    vec.erase(it);
    return picked;
}

#endif //DPLL_UTILS_H
