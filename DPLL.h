//
// Created by tanyi on 2022.08.06..
//

#ifndef DPLL_DPLL_H
#define DPLL_DPLL_H

#include "Formula.h"

void removeUnits(Formula &formula);

void removeMonotones(Formula &formula);

bool checkEmpty(Formula &formula);

bool decide(Formula formula, fVariable variable);

bool dpll(Formula &formula);

#endif //DPLL_DPLL_H
