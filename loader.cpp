//
// Created by tanyi on 2022.08.08..
//

#include "loader.h"

void load(const std::string &filename, Formula &formula) {
    std::string line, word;
    std::ifstream file(filename);
    if (!file.is_open()) {
        throw std::exception();
    }
    std::size_t lineCount=0;
    while(getline(file, line)) {
        lineCount++;
        if (isNumber(line))
        {
            std::stringstream linestream(line);
            fClause clause;
            while (getline(linestream, word, ' ')) {
                if (isNumber(word))
                    clause.push_back(stoi(word));
            }
            formula.addClause(clause);
        }
    }
    file.close();
}

bool isNumber(std::string &line) {
    return (line[0] >= '1' && line[0] <='9'
            ||
            line[0] == '-' && line.size() > 1 && line[1] >= '1' && line[1] <= '9');
}