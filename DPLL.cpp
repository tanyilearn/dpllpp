//
// Created by tanyi on 2022.08.06..
//

#include "DPLL.h"

void removeUnits(Formula &formula) {

    fLiteral unit;
    std::vector<fClause> *clauses = &formula.getClauses();
    while (true) {

        unit = 0;
        auto cit = clauses->begin();

        while (cit != clauses->end()) {
            if (cit->size() == 1) {
                unit = (*cit)[0];
                clauses->erase(cit);
                break;
            } else
                ++cit;
        }


        if (unit != 0) {

            auto clause = clauses->begin();
            while (clause != clauses->end()) {

                auto l = clause->begin();

                while (l != clause->end()) {
                    if (*l == unit) {
                        // remove the clause & break loop 'l'
                        clause-- = clauses->erase(clause);
                        break;
                    } else if (*l == -unit) {
                        // remove unit from closes
                        l = clause->erase(l);
                    } else {
                        l++;
                    }

                }
                clause++;
            }

        } else
            break;

    }
}

void removeMonotones(Formula &formula) {

    std::vector<fClause> &clauses = formula.getClauses();
    std::set<fLiteral> removable;
    for (fClause c : clauses) {
        for (fLiteral l : c) {
            if (find(removable.begin(), removable.end(), l * -1) != removable.end())
                removable.insert(l);
            else
                removable.erase(l * -1);
        }
    }

    while (removable.size() > 0) {
        fLiteral pure = popFromSet(removable);
        auto c = clauses.begin();
        while (c != clauses.end()) {
            auto l = c->begin();
            while (l != c->end()) {
                if (*l == pure) {
                    c-- = clauses.erase(c);
                    break;
                } else {
                    l++;
                }
            }
            c++;
        }
    }
}

bool checkEmpty(Formula &formula) {
    for (fClause c : formula.getClauses())
        if (c.size() == 0)
            return true;
    return false;
}

bool decide(Formula formula, fVariable variable) {
    formula.addLiteral(variable);
    return dpll(formula);
}

bool dpll(Formula &formula) {

    removeUnits(formula);

    removeMonotones(formula);

    // check if empty set formula
    if (formula.getClauses().size() == 0)
        return true;

    // check if empty clause exists
    if (checkEmpty(formula))
        return false;

    // pick one
    fVariable v = formula.pickVariable();

    // test literal or not literal
    return (decide(formula, v)) || (decide(formula, v * -1));
}
