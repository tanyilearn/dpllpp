//
// Created by tanyi on 2022.08.06..
//

#include "Formula.h"

void Formula::addClause(fClause clause) {
    for (fLiteral l : clause)
        variables.insert(abs(l));
    clauses.push_back(clause);
}

void Formula::addLiteral(fLiteral literal) {
    clauses.push_back({literal});
}

//void Formula::removeClause(fClause clause) {
//    std::vector<fClause>::iterator it = clauses.begin();
//    while (it != clauses.end()) {
//        if (*it == clause)
//            it = clauses.erase(it);
//        else
//            ++it;
//    }
//
//}

std::vector<fClause> &Formula::getClauses() {
    return clauses;
}

fVariable Formula::pickVariable() {
    return popFromSet(variables);
}







